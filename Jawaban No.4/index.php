<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">YKM</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Home</a></li>
      <li><a href="#">Artikel</a></li>
      <li><a href="#">About</a></li>
     
    </ul>
  </div>
</nav>

<div class="container">
  <h2>Berita Hari ini</h2>
  <p>Terdepan dalam penyajian Berita</p>            
  
<?php
$servername = "localhost";
$username = "homestead";
$password = "secret";
$dbname = "blog";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}


$sql = 'SELECT c.*, p.title,p.content FROM posts p INNER JOIN comments c ON p.id = c.postId ORDER BY p.id DESC, c.postId DESC';

$result = mysqli_query($conn, $sql);
$previous_post = 0;
while ($data = mysqli_fetch_assoc($result)) {
    if ($previous_post != $data['postId']) {
        echo '<h2>' . $data['title'] . '</h2>';
        echo '<p>' . $data['content'] . '</p>';

        $previous_post = $data['postId'];
        echo "<b>". "Comment : " . "</b><br>";
    }
    echo '<p>' . $data['comment'] . '</p>';
}

?>

</div>
<div class="footer navbar-fixed-bottom" >
     
        <span>@Copyright 2018 - Test React Native</span>
     
</footer>
</body>
</html>